package com.proop.lab1;

import com.proop.lab1.container.ShapeContainer;
import com.proop.lab1.painter.CirclePainter;
import com.proop.lab1.painter.EllipsePainter;
import com.proop.lab1.painter.LinePainter;
import com.proop.lab1.painter.RectanglePainter;
import com.proop.lab1.painter.SquarePainter;
import com.proop.lab1.painter.TrianglePainter;
import com.proop.lab1.shape.Circle;
import com.proop.lab1.shape.Ellipse;
import com.proop.lab1.shape.Line;
import com.proop.lab1.shape.Rectangle;
import com.proop.lab1.shape.Shape;
import com.proop.lab1.shape.Square;
import com.proop.lab1.shape.Triangle;

public class App {

    public static void main(String... args) {
        ShapeContainer shapeContainer = new ShapeContainer();
        shapeContainer.add(new Circle(1, 1, 1, new CirclePainter()));
        shapeContainer.add(new Ellipse(1, 1, 1, 2, new EllipsePainter()));
        shapeContainer.add(new Line(1, 1, 2, 2, new LinePainter()));
        shapeContainer.add(new Rectangle(1, 1, 2, 2, new RectanglePainter()));
        shapeContainer.add(new Square(0, 0, 1, 1,  new SquarePainter()));
        shapeContainer.add(new Triangle(0, 0, 1, 0, 1, 1,  new TrianglePainter()));
        shapeContainer.getShapes().forEach(Shape::paint);
    }

}
