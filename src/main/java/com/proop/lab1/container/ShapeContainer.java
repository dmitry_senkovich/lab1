package com.proop.lab1.container;

import com.proop.lab1.shape.Shape;

import java.util.ArrayList;
import java.util.List;

public class ShapeContainer {

    private List<Shape> shapes = new ArrayList<>();

    public void add(Shape shape) {
        shapes.add(shape);
    }

    public List<Shape> getShapes() {
        return shapes;
    }

}
