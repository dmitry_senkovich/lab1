package com.proop.lab1.painter;

public class CirclePainter implements Painter {

    @Override
    public void paint(Integer[] coordinates) {
        System.out.println(String.format("Circle((%d, %d), %d)",
                coordinates[0], coordinates[1], coordinates[2]));
    }

}
