package com.proop.lab1.painter;

public class EllipsePainter implements Painter {

    @Override
    public void paint(Integer[] coordinates) {
        System.out.println(String.format("Ellipse((%d, %d), %d, %d)",
                coordinates[0], coordinates[1], coordinates[2], coordinates[3]));
    }

}
