package com.proop.lab1.painter;

public class LinePainter implements Painter {

    @Override
    public void paint(Integer[] coordinates) {
        System.out.println(String.format("Line((%d, %d), (%d, %d))",
                coordinates[0], coordinates[1], coordinates[2], coordinates[3]));
    }

}
