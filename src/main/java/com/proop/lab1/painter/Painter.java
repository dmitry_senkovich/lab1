package com.proop.lab1.painter;

public interface Painter {

    void paint(Integer[] coordinates);

}
