package com.proop.lab1.shape;

import com.proop.lab1.painter.Painter;

public class Circle implements Shape {

    private Integer x;
    private Integer y;
    private Integer r;

    private Painter painter;

    public Circle(Integer x, Integer y, Integer r, Painter painter) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.painter = painter;
    }

    public void paint() {
        painter.paint(getCoordinates());
    }

    public Integer[] getCoordinates() {
        return new Integer[] {x, y, r};
    }
}
