package com.proop.lab1.shape;

import com.proop.lab1.painter.Painter;

public class Ellipse implements Shape {

    private int x;
    private int y;
    private int a;
    private int b;
    private Painter painter;

    public Ellipse(int x, int y, int a, int b, Painter painter) {
        this.x = x;
        this.y = y;
        this.a = a;
        this.b = b;
        this.painter = painter;
    }

    @Override
    public void paint() {
        painter.paint(getCoordinates());
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{x, y, a, b};
    }

}
