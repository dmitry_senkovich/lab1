package com.proop.lab1.shape;

public interface Shape {

    void paint();

    Integer[] getCoordinates();

}
