package com.proop.lab1.shape;

import com.proop.lab1.painter.Painter;

public class Square implements Shape {

    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private Painter painter;

    public Square(int x1, int y1, int x2, int y2, Painter painter) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.painter = painter;
    }

    @Override
    public void paint() {
        painter.paint(getCoordinates());
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{x1, y1, x2, y2};
    }

}
